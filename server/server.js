var express = require('express');
var app = express();
var server = require('http').createServer(app);
var io = require('socket.io')(server);
var port = process.env.PORT || 80;
var util = require("util");
var request = require('request');
var Telnet = require('util-telnet');
var pjlink = require('pjlink');


var config = {
    host: '192.168.0.25',
    port: 23,
    username: 'admin',
    password: '12345678',
    enpassword: '',
    log:true
}
var c = new Telnet();
var c2 = new Telnet();

server.listen(port, function () {
  console.log('Server listening at port %d', port);
});

app.use(express.static(__dirname + '/public'));


io.on('connection', function(socket){
	socket.on('dmx', function(data){
		console.log("GETING DMX CONTROL " + data.control);
			var switcher = data.control;
			if (switcher=="ON"){
				console.log("turn on");
				c.connect(config);
				c.on('data', function (data) {
					    c.write('admin:12345678\n');
					    c.write('setpower p61=0\n');
					    c.write('exit\n');
				});
			}
			else if (switcher=="OFF"){
				console.log("turn off")
				c2.connect(config);
				c2.on('data', function (data) {
					    c2.write('admin:12345678\n');
					    c2.write('setpower p61=1\n');
					    c2.write('exit\n');
				});
			}
	});

});
