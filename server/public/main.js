var allowTouchs = true;
var backAllowed = false;
var backToHotSpots = false;
var start = true;
var counter = 0;
var $window = $(window);
var socket = io();
var pop = Popcorn("#videoPLayer");
pop.volume(0.4);
var snd1 = new Audio("sounds/sounds1.mp3"); // buffers automatically when created


$(function() {

	$("#hotspot1Info").hide();
	$("#hotspot1Info").css("background-color","rgba(0, 160, 227, 0.85)");
	$("#hotspot2Info").hide();
	$("#hotspot2Info").css("background-color","rgba(0, 160, 227, 0.85)");
	$("#hotspot3Info").hide();
	$("#hotspot3Info").css("background-color","rgba(0, 160, 227, 0.85)");
	$("#hotspot1").hide();
	$("#hotspot2").hide();
	$("#hotspot3").hide();
	$("#myDiv2").show();
	pop.currentTime(1);
	pop.pause();
	$("#header").hide();
	$("#banner").hide();
	$("#footer").hide();
	$('#arrow').attr("src", "img/arrow.png");
});

socket.on('connect', function () { 
	console.log("connected socket");
	socket.emit("dmx", {control : "ON"});
	socket.emit('message', {control : "pause"});
	$window.bind('keydown', function(e){
		if(e.keyCode == 34){
			console.log(e.keyCode);
			if(allowTouchs){
				console.log(counter);
				if(counter == 0){
					$("#myDiv2").show();
					$('#playme').hide();
					$("body").css('background-color','#fff');
					pop.play()
					counter=1;
					socket.emit('message', {control : "pause"});
					socket.emit("dmx", {control : "ON"});
				}
				else if(counter == 1){
					clearInfo();
					$('#playme').hide();
					$("#myDiv2").hide();
					$("body").css('background-color','#000');
					pop.pause()
					counter=0;
					socket.emit('message', {control : "play"});
					socket.emit("dmx", {control : "OFF"});
				}
			}
		}
	});
});

function commaSeparateNumber(val){
	while (/(\d+)(\d{3})/.test(val.toString())){
		val = val.toString().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,");
	}
	return val;
}

function commaSeparateNumberPerCent(val){
	while (/(\d+)(\d{3})/.test(val.toString())){
		val = val.toString().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,");
	}
	return val + "%";
}


	function clearInfo(){
		$("#myDiv2").hide();
		$("#header").hide();
		$("#banner").hide();
		$("#footer").hide();
		
	}

